package com.alter.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author alter
 * @Date 2019/11/4 16:04
 * @Version 1.0
 **/
@Api(value = "商品订单", description = "商品订单")
@RestController
@RequestMapping("/api/goods")
public class TestController {

    @RequestMapping("/list")
    @ApiOperation(value = "hello接口", notes = "hello接口" , httpMethod = "GET")
    public String index() {
        return "hello world";
    }

}
